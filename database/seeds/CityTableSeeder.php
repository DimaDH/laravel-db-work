<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('cities')->insert([
            'city_name' => 'Винница'
        ]);
        DB::table('cities')->insert([
            'city_name' => 'Луцк'
        ]);
        DB::table('cities')->insert([
            'city_name' => 'Днепр'
        ]);
        DB::table('cities')->insert([
            'city_name' => 'Донецк'
        ]);
        DB::table('cities')->insert([
            'city_name' => 'Житомир'
        ]);
        DB::table('cities')->insert([
            'city_name' => 'Ужгород'
        ]);
        DB::table('cities')->insert([
            'city_name' => 'Запорожье'
        ]);
        DB::table('cities')->insert([
            'city_name' => 'Ивано-Франковск'
        ]);
        DB::table('cities')->insert([
            'city_name' => 'Киев'
        ]);
        DB::table('cities')->insert([
            'city_name' => 'Кропивницкий'
        ]);
        DB::table('cities')->insert([
            'city_name' => 'Луганск'
        ]);
        DB::table('cities')->insert([
            'city_name' => 'Львов'
        ]);
        DB::table('cities')->insert([
            'city_name' => 'Николаев'
        ]);
        DB::table('cities')->insert([
            'city_name' => 'Одесса'
        ]);
        DB::table('cities')->insert([
            'city_name' => 'Полтава'
        ]);
        DB::table('cities')->insert([
            'city_name' => 'Ровно'
        ]);
        DB::table('cities')->insert([
            'city_name' => 'Сумы'
        ]);
        DB::table('cities')->insert([
            'city_name' => 'Тернополь'
        ]);
        DB::table('cities')->insert([
            'city_name' => 'Харьков'
        ]);
        DB::table('cities')->insert([
            'city_name' => 'Херсон'
        ]);
        DB::table('cities')->insert([
            'city_name' => 'Хмельницкий'
        ]);
        DB::table('cities')->insert([
            'city_name' => 'Черкассы'
        ]);
        DB::table('cities')->insert([
            'city_name' => 'Чернигов'
        ]);
        DB::table('cities')->insert([
            'city_name' => 'Черновцы'
        ]);
    }

}
