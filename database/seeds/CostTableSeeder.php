<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('costs')->insert([
            'cost' => '2000 uah'
        ]);
        DB::table('costs')->insert([
            'cost' => '4000 uah'
        ]);
        DB::table('costs')->insert([
            'cost' => '5000 uah'
        ]);
        DB::table('costs')->insert([
            'cost' => '10000 uah'
        ]);
        DB::table('costs')->insert([
            'cost' => '20000 uah'
        ]);
    }
}
