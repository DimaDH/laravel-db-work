<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class RoomTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('rooms')->insert([
            'amount_room' => '1'
        ]);
        DB::table('rooms')->insert([
            'amount_room' => '2'
        ]);
        DB::table('rooms')->insert([
            'amount_room' => '3'
        ]);
        DB::table('rooms')->insert([
            'amount_room' => '4'
        ]);
        DB::table('rooms')->insert([
            'amount_room' => '5'
        ]);
    }
}
