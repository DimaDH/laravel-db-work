<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FloorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('floors')->insert([
            'floor' => '1'
        ]);
        DB::table('floors')->insert([
            'floor' => '2'
        ]);
        DB::table('floors')->insert([
            'floor' => '3'
        ]);
        DB::table('floors')->insert([
            'floor' => '4'
        ]);
        DB::table('floors')->insert([
            'floor' => '5'
        ]);
        DB::table('floors')->insert([
            'floor' => '6'
        ]);
        DB::table('floors')->insert([
            'floor' => '7'
        ]);
        DB::table('floors')->insert([
            'floor' => '8'
        ]);
        DB::table('floors')->insert([
            'floor' => '9'
        ]);
        DB::table('floors')->insert([
            'floor' => '15'
        ]);
        DB::table('floors')->insert([
            'floor' => '16'
        ]);
    }
}
