<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TypeRendTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('type_rends')->insert([
            'type_rend' => 'посуточно'
        ]);
        DB::table('type_rends')->insert([
            'type_rend' => 'почасовая'
        ]);
        DB::table('type_rends')->insert([
            'type_rend' => 'долгосрочная'
        ]);
    }
}
