<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cost extends Model
{
    protected $table = 'costs';

    public function costs() {
        return $this->belongsTo('App\House');
    }
}
