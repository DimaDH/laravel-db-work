<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $table = 'rooms';

    public function rooms() {
        return $this->belongsTo('App\House');
    }
}
