<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeRend extends Model
{
    protected $table = 'type_rends';

    public function typeRends() {
        return $this->belongsTo('App\House');
    }
}
