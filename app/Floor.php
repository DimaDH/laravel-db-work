<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Floor extends Model
{
    protected $table = 'floors';

    public function floors() {
        return $this->belongsTo('App\House');
    }
}
